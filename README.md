# Spring Boot Static Single Page Application Example

> Example project demonstrating how to embed a single page application in a Spring Boot web application. 

## Overview

The goal of this example project is to demonstrate:
 - How a single page application (SPA) can be embedded in a Spring Boot web application.
 - How to setup GitLab Triggers to trigger a Spring Boot build via maven and deploy to a Virtual Machine in AWS.

## Red Hat Entperise Linux Dependancies

- yum update
- git
- OpenJDK-13
- maven
- python3

## Running the Application

1. Clone this repository.
2. Open a terminal in the project directory.
3. Run the command:

    ```
    bash
    ./mvnw spring-boot:run
    ```

or

    ```
    maven clean package
    cd target
    java -jar spring-boot-embedded-spa-example-0.0.1-SNAPSHOT.jar
    ```

4. Visit: <http://localhost:8090/>
5. Try the HTTP requests located in `test.http`

## Application Reminders

- The webpage can be edited in `tcsg-spring/src/main/resources/templates/index.mustache`
- App configuration can be edited in `tcsg-spring/src/main/resources/application.properties`
  - To run on localhost you should change `server.address` from `0.0.0.0` to `127.0.0.1`
  - Server port is set to `8090`. When navigating to <URL> add `:8090`

## License

[MIT](https://opensource.org/licenses/MIT)
